export class JwtResponse {
  token: string;
  type: string;
  username: string;
  fullName: string;
  rol: string;

  constructor(token: string = '', type: string = '', username: string = '', fullName: string = '', rol: string = '') {
    this.token = token;
    this.type = type;
    this.username = username;
    this.fullName = fullName;
    this.rol = rol;
  }
}
