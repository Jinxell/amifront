export interface GeneralBodyResponse {
  message: string;
  data: any;
  errors: any[];
  auxErrorCode: number;
}
