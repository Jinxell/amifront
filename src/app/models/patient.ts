export interface Patient {
  id: number;
  birthday: string;
  bpressure: string;
  gender: string;
  height: number;
  name: string;
  weight: number;
}
