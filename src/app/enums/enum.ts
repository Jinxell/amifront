export enum RoutesPathEnum {
  LOGIN = '/login',
  ERROR_LOGIN = '/expiry-session',
  MODULE = '/module',
}

export enum EventEnum {
  SAVE = 'SAVE',
  UPDATE = 'UPDATE',
  DELETE = 'DELETE',
  CANCEL = 'CANCEL',
  CREATE = 'CREATE',
  NONE = 'NONE'
}

export enum PositionEnum {
  start = 'start',
  center = 'center',
  bottom = 'bottom',
  top = 'top'
}

export enum DateFormatEnum {
  ANIO_MES_DIA = 'yyyy-MM-dd',
}

export enum AlertClassEnum {
  DANGER = 'danger-snackbar',
  SUCCESS = 'succes-snackbar',
  WARNING = 'warning-snackbar',
  INFO = 'info-snackbar'
}

export enum ConsoleLogTypeEnum {
  Log = 1,
  Warning = 2,
  Error = 3,
}

export enum NumberEnum {
  zero = 0,
  one = 1,
  two = 2,
  three = 3,
  four = 4,
  five = 5
}

export enum RolesEnum {
  CONSULTA_ADMON_LISTA_PREVEN = 'CONSULTA_ADMON_LISTA_PREVEN',
  CONSULTA_OFIC_LISTA_PREVEN = 'CONSULTA_OFIC_LISTA_PREVEN',
  ADMINISTRADOR_LISTA_PREV_DUCC = 'ADMINISTRADOR_LISTA_PREV_DUCC',
  ADMON_LISTAS_PREVENCION = 'ADMON_LISTAS_PREVENCION',
  ADMON_LISTA_EXTRANJEROS = 'ADMON_LISTA_EXTRANJEROS'
}
