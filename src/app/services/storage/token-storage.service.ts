import {Injectable} from '@angular/core';

const TOKEN_KEY = 'token';
const USERNAME_KEY = 'username';
const ROL_KEY = 'rol';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {
  private roles: Array<string> = [];

  constructor() {
  }

  signOut() {
    window.sessionStorage.clear();
    window.localStorage.clear();
    window.location.reload(); // f5
  }

  public saveToken(token: string) {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string {
    return <string>sessionStorage.getItem(TOKEN_KEY);
  }

  public saveUsername(username: string) {
    window.sessionStorage.removeItem(USERNAME_KEY);
    window.sessionStorage.setItem(USERNAME_KEY, username);
  }

  public getUsername(): string {
    return <string>sessionStorage.getItem(USERNAME_KEY);
  }

  public saveRol(rol: string) {
    window.sessionStorage.removeItem(ROL_KEY);
    window.sessionStorage.setItem(ROL_KEY, rol);
  }

  public getRol(): string {
    return <string>sessionStorage.getItem(ROL_KEY);
  }

  getKeyValue(key: string): string {
    const keyValue = this.getUsername().toLowerCase() + '-' + key;
    return keyValue;
  }
}
