import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {AuthenticationService} from "../storage/authentication.service";
import {Observable} from "rxjs";
import {GeneralBodyResponse} from "../../models/general-body-response";
import {Patient} from "../../models/patient";

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient, private authenticationService: AuthenticationService) {
  }

  async findAllPatients() {
    const path = environment.url + 'patients';
    const headers = this.authenticationService.getHeader();

    return await this.http.get<any>(path, {headers}).toPromise();
  }

  create(dto: Patient): Observable<GeneralBodyResponse> {
    const path = environment.url + 'patient';
    const headers = this.authenticationService.getHeader();
    return this.http.post<GeneralBodyResponse>(path, dto, {headers});
  }

  update(dto: Patient): Observable<GeneralBodyResponse> {
    const path = environment.url + 'patient/' + dto.id;
    const headers = this.authenticationService.getHeader();
    return this.http.put<GeneralBodyResponse>(path, dto, {headers});
  }
//////////////////////////////////////////////////////////////////////////////

  delete(id: number): Observable<GeneralBodyResponse> {
    const path = environment.url + 'patient/' + id;
    const headers = this.authenticationService.getHeader();
    return this.http.delete<GeneralBodyResponse>(path, {headers});
  }

  searchPatients(keyword: string): Observable<GeneralBodyResponse> {
    const path = environment.url + 'patient-search';
    const headers = this.authenticationService.getHeader();

    return this.http.post<any>(path, {'name': keyword}, {headers});
  }






}
