import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {AuthLoginInfo} from "../../models/login/auth-login-info";
import {JwtResponse} from "../../models/login/jwt-response";

const headerOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class LoginService { // el encargado de ir a traer la request de un endpoint tipo REST

  constructor(private http: HttpClient) {
  }

  login(body: AuthLoginInfo): Observable<JwtResponse> {
    const path = environment.url + 'auth';
    return this.http.post<JwtResponse>(path, body, headerOptions);
  }

}
