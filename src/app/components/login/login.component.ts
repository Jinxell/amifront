import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {LoginService} from "../../services/login/login.service";
import {AuthLoginInfo} from "../../models/login/auth-login-info";
import {TokenStorageService} from "../../services/storage/token-storage.service";
import {JwtResponse} from "../../models/login/jwt-response";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: any = {}; // json
  private loginInfo: AuthLoginInfo | undefined;
  // JwtResponse
  oJwtResponse = new JwtResponse();

  constructor(
    private loginService: LoginService,
    private tokenStorage: TokenStorageService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  login() {

    this.loginInfo = new AuthLoginInfo(this.form.username, this.form.password);

    console.log('prueba de logeo', this.loginInfo)

    this.loginService.login(this.loginInfo).subscribe(
      data => {
        console.log(data)
        this.oJwtResponse.token = data.token;
        this.oJwtResponse.rol = data.rol;
        this.oJwtResponse.username = data.username;
        this.oJwtResponse.type = data.type;
        this.oJwtResponse.fullName = data.fullName;
        this.setLocalStorage();
        this.router.navigate(['/dashboard']);
      },
      error => {

      });
  }

  setLocalStorage() {
    // Guardando usuario en el session storage
    this.tokenStorage.saveToken(this.oJwtResponse.token);
    this.tokenStorage.saveUsername(this.oJwtResponse.username);
    this.tokenStorage.saveRol(this.oJwtResponse.rol);
  }
}
