import {NgModule} from '@angular/core';
import {LoginComponent} from "./login.component";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {RouterModule, Routes} from "@angular/router";
import {DashboardModule} from "../dashboard/dashboard.module";
import {MatDialogModule} from "@angular/material/dialog";
import {DialogComponent} from "../utils/dialog.component";

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  }
];

@NgModule({
  declarations: [
    LoginComponent,
    DialogComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    DashboardModule,
    MatDialogModule
  ],
  exports: [LoginComponent,DialogComponent]
})
export class LoginModule {
}
