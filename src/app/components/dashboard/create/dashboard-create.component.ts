import {Component, Input, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

import {DashboardService} from "../../../services/dashboard/dashboard.service";
import {MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition} from "@angular/material/snack-bar";
import {AlertClassEnum, NumberEnum, PositionEnum} from "../../../enums/enum";
import {Patient} from "../../../models/patient";



@Component({
  selector: 'app-dashboard-create',
  templateUrl: './dashboard-create.component.html',
  styleUrls: ['./dashboard-create.component.css']
})
export class DashboardCreateComponent implements OnInit {
  @Input() cleanFm: Boolean =false;
  @Input() update!: Patient;
  form!: FormGroup;
  horizontalPosition: MatSnackBarHorizontalPosition = PositionEnum.center;
  verticalPosition: MatSnackBarVerticalPosition = PositionEnum.top;
  label: String = "Guardar";

  constructor(private formBuilder: FormBuilder,
              private dash: DashboardService,
              private _snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.label = this.update.id != 0 ? 'Actualizar' : 'Guardar';

    this.form = this.formBuilder.group({
      id: [this.update.id == 0 ? null : this.update.id],
      birthday: [this.update.birthday, Validators.required],
      bpressure: [this.update.bpressure, Validators.required],
      gender: [this.update.gender, Validators.required],
      height: [this.update.height, Validators.required],
      name: [this.update.name, Validators.required],
      weight: [this.update.weight, Validators.required]

    });
    if (this.cleanFm && this.update.id != null){
      this.clearForm();
    }

  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.form.controls[controlName];

    if (!control)
      return false;
    else
      return control.hasError(validationType) && (control.dirty || control.touched);
  }

  clearForm() {
    this.update = {
      id: 0,
      birthday: "",
      bpressure:"",
      gender:"",
      height: 0,
      name: "",
      weight: 0
    }

    this.form = this.formBuilder.group({
      id: [this.update.id == 0 ? null : this.update.id],
      birthday: [this.update.birthday, Validators.required],
      bpressure: [this.update.bpressure, Validators.required],
      gender: [this.update.gender, Validators.required],
      height: [this.update.height, Validators.required],
      name: [this.update.name, Validators.required],
      weight: [this.update.weight, Validators.required]
    });
  }



  create(form: FormGroup) {

    let dto: Patient;

    if (form.value) {
      const data = {
        id: form.value.id,
        birthday: form.value.birthday,
        bpressure: form.value.bpressure,
        gender: form.value.gender,
        height: form.value.height,
        name: form.value.name,
        weight: form.value.weight
      }
      // @ts-ignore
      dto = data as Pantient;

      if (form.value.id != null) {
        this.dash.update(dto).subscribe(
          _response => {
            this.launchSnackBar('ok', 'actualizado', AlertClassEnum.SUCCESS);
            // this.router.navigate([RoutesPathEnum.MODULE, 'excbbconslisneg', 1]);
          },
          error => {
            // this.extracted(error);
            this.clearForm();
          }
        );
      } else {
        this.dash.create(dto).subscribe(
          _response => {
            this.launchSnackBar('ok', 'creado', AlertClassEnum.SUCCESS);
            // this.router.navigate([RoutesPathEnum.MODULE, 'excbbconslisneg', 1]);
            this.clearForm();
          },
          error => {
            // this.extracted(error);
            this.clearForm();
          }
        );
      }
    } else {
      this.launchSnackBar('FAIL', 'error formulario', AlertClassEnum.WARNING);
    }
  }

  private launchSnackBar(msg: string, action: string, type: string) {
    this._snackBar.open(msg, action, {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      duration: NumberEnum.four * 1000,
      panelClass: [type]
    });
  }
}
