import {Component, OnInit} from '@angular/core';
import {DashboardService} from "../../services/dashboard/dashboard.service";
import {AuthenticationService} from "../../services/storage/authentication.service";
import {Patient} from "../../models/patient";
import {MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition} from "@angular/material/snack-bar";
import {AlertClassEnum, NumberEnum, PositionEnum} from "../../enums/enum";
import {MatDialog} from "@angular/material/dialog";
import {DialogComponent} from "../utils/dialog.component";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  displayedColumns: string[] = ['id', 'birthday', 'bpressure', 'gender', 'height', 'name', 'weight', 'actions'];
  listPatient: Patient[] = [];
  viewCreate: boolean = false;
  updateSend!: Patient;
  horizontalPosition: MatSnackBarHorizontalPosition = PositionEnum.center;
  verticalPosition: MatSnackBarVerticalPosition = PositionEnum.top;
  searchKeyWord = "";


  constructor(private dashboardService: DashboardService,
              private auth: AuthenticationService,
              private _snackBar: MatSnackBar,
              public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.updateSend = {
      id: 0,
      birthday: "",
      bpressure:"",
      gender:"",
      height: 0,
      name: "",
      weight: 0
    }
    this.getAllPatients();
  }

  async getAllPatients() {
    const response = await this.dashboardService.findAllPatients().catch((err) => {
      console.log(err);
    });

    if (response) {
      console.log(response.data);
      this.listPatient = response.data;
    }
  }

  logout() {
    this.auth.logout();
  }

  changeView(view: boolean, patient: Patient = this.updateSend) {
    this.viewCreate = view;

    if (!this.viewCreate)
      this.getAllPatients();

    if (patient.id != 0)
      this.updateSend = patient;
  }
//////////////////////////////////////////////////////////////////////////////////

  deleteById(id: number) {
    const dialogRef = this.dialog.open(DialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.dashboardService.delete(id).subscribe(data => {
          console.log(data);
          this.launchSnackBar('OK', 'se elimino el paciente con id ' + id, AlertClassEnum.SUCCESS);
          this.clean();
        }, error => {
          console.log(error);
          this.launchSnackBar('fail', 'Fallo el eliminar el paciente con id ' + id, AlertClassEnum.DANGER);
        });
      }

    });
  }

  search() {
    if (this.searchKeyWord !== "") {
      this.dashboardService.searchPatients(this.searchKeyWord).subscribe(response => {
        console.log(response);
        this.launchSnackBar('OK', 'Se encontraron resultados ', AlertClassEnum.SUCCESS);
        this.listPatient = [];
        this.listPatient = response.data;
      }, error => {
        console.log(error);
        this.launchSnackBar('fail', 'No se encontraron resultados', AlertClassEnum.DANGER);
      });
    } else {
      this.launchSnackBar('Fail', 'debe llenar los campos obligatorios', AlertClassEnum.DANGER);
    }
  }

  clean() {
    this.searchKeyWord = "";
    this.getAllPatients();
  }

  private launchSnackBar(msg: string, action: string, type: string) {
    this._snackBar.open(msg, action, {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      duration: NumberEnum.four * 1000,
      panelClass: [type]
    });
  }



}
