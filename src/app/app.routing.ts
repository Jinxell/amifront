import { Routes, RouterModule } from '@angular/router';

const APP_ROUTES: Routes = [
  {
    path: 'login',
    loadChildren: './components/login/login.module#LoginModule'
  },
  {
    path: '**',
    redirectTo: '/login'
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { useHash: true });
